'use strict';
/* eslint-disable */

const SensorChannels = [
  'LIS3DH',
  'HMC5883L',
  'BME280',
  'TSL2591',
  'SI1145',
  'DS18B20',
  'GPS',
  'heartbeat',
  'system',
  'camera'
];

let subscribed = false;

Chart.pluginService.register({
  beforeDraw: function (chart, easing) {
    if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
      let helpers = Chart.helpers;
      let ctx = chart.chart.ctx;
      let chartArea = chart.chartArea;

      ctx.save();
      ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
      ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
      ctx.restore();
    }
  }
});

Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};

Number.prototype.clamp = function(min, max) {
  return Math.min(Math.max(this, min), max);
};

let sensors = {
  environment: {
    chart: null,
    dataTypes: {
      temperature: 0,
      humidity: 1,
      pressure: 2,
      altitude: 3
    }
  },
  compass: {
    chart: null
  },
  systemInfo: {
    chart: null
  },
  light: {
    chart: null,
    dataTypes: {
      ir: 0,
      visibility: 1,
      uv: 2
    }
  },
  accelerometer: {
    chart: null,
    dataTypes: {
      accelX: 0,
      accelY: 1,
      accelZ: 2
    }
  },
  altitude: {
    chart: null,
    dataTypes: {
      altitude: 0,
      temperature: 1
    }
  }
};

let client = null;

const MAX_DATA_POINTS = 50;

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateEnvironmentData() {
  return setInterval(function() {
    let now = Date.now();
    addDataPoint('environment', 'temperature', now, getRandomInt(-55, 35));
    addDataPoint('environment', 'humidity',    now, getRandomInt(65, 100));
    addDataPoint('environment', 'pressure',    now, getRandomInt(25, 75));
  }, 500);
}

function pointCompass(degrees) {
  let arrow = document.getElementById('compassArrowImg');
  arrow.style.webkitTransform = `rotate(${degrees}deg)`;
  arrow.style.MozTransform = `rotate(${degrees}deg)`;
  arrow.style.transform = `rotate(${degrees}deg)`;
}

/**
 *
 * @param sensorType
 * @param dataType
 * @param time
 * @param entry
 */
function addDataPoint(sensorType, dataType, time, entry) {
  let sensor = sensors[sensorType];
  let chart = sensor.chart;
  let type = sensor.dataTypes[dataType];
  let data = chart.data.datasets[type].data;

  data.push({
    x: time,
    y: entry
  });

  if (data.length > MAX_DATA_POINTS) {
    data.shift();
  }

  chart.update();
}

function createSystemInfoChart() {

  let systemInfoData = {
    labels: ['CPU (% busy)', 'RAM (% free)', 'Disk (% free)'],
    datasets: [{
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      borderColor: '#ffffff',
      borderWidth: 1,
      data: [
        0, 0, 0
      ]
    }]

  };

  let ctx = document.getElementById('chart-systeminfo').getContext('2d');
  return new Chart(ctx, {
    type: 'horizontalBar',
    data: systemInfoData,
    options: {
      // Elements options apply to all of the options unless overridden in a dataset
      // In this case, we are setting the border of each horizontal bar to be 2px wide
      elements: {
        rectangle: {
          borderWidth: 2,
        }
      },
      scales: {
        xAxes: [{
          ticks: {
            min: 0,
            max: 100
          }
        }]
      },
      responsive: true,
      height: 300,
      legend: false,
      title: {
        display: false,
        text: 'Chart.js Horizontal Bar Chart'
      }
    }
  });
}

/**
 * Create a chart, given its related sensor-type and datasets.
 * @param sensorType
 * @param datasets
 * @param chartType
 * @param options
 * @returns {*}
 */
function createChart(sensorType, datasets, chartType, options) {
  let ctx = document.getElementById(`chart-${sensorType}`);
  let chart = null;

  switch (chartType) {
    case 'line': {
      chart = new Chart(ctx, {
        type: chartType,
        responsive: true,
        maintainAspectRatio: false,
        options: Object.assign({
          scales: {
            xAxes: [{
              type: 'time',
              time: {
                unit: 'second'
              }
            }],
            yAxes: [{
              ticks: {
                min: -100,
                max: 100,
                beginAtZero: true,
                stacked: true
              }
            }]
          }
        }, options)
      });

      for (const dataset of datasets) {
        chart.data.datasets.push({
          label: dataset.label,
          backgroundColor: [dataset.backgroundColor],
          borderColor: [dataset.borderColor]
        });
      }
    } break;

    case 'doughnut': {
      chart = new Chart(ctx, {
        type: chartType,
        data: {
          datasets: [{
            data: [360]
          }],
          labels: [ 'Degrees' ]
        },
        options: Object.assign({
          legend: {
            display: false
          },
          cutoutPercentage: 84,
          rotation:         -0.5 * Math.PI,
          circumference:    2 * Math.PI,
          animation:        {animateRotate: false}
        }, options)
      });
    } break;

    default: {
      throw new Error(`Unsupported chart type ${chartType}`);
    } break;
  }

  chart.update();

  return chart;
}


/**
 * Create a chart, given its related sensor-type and datasets.
 * @param sensorType
 * @param datasets
 * @param chartType
 * @returns {*}
 */
function createAccelerometerChart(sensorType, datasets, chartType) {
  let ctx = document.getElementById(`chart-${sensorType}`);
  let chart = null;

  switch (chartType) {
    case 'line': {
      chart = new Chart(ctx, {
        type: chartType,
        responsive: true,
        maintainAspectRatio: false,
        options: {
          scales: {
            xAxes: [{
              type: 'time',
              time: {
                unit: 'second'
              }
            }],
            yAxes: [{
              ticks: {
                min: -2,
                max: 2,
                beginAtZero:true,
                stacked: true
              }
            }]
          }
        }
      });

      for (const dataset of datasets) {
        chart.data.datasets.push({
          label: dataset.label,
          backgroundColor: [dataset.backgroundColor],
          borderColor: [dataset.borderColor]
        });
      }
    } break;

    default: {
      throw new Error(`Unsupported chart type ${chartType}`);
    } break;
  }

  chart.update();

  return chart;
}

/**
 * Create a chart, given its related sensor-type and datasets.
 * @param sensorType
 * @param datasets
 * @param chartType
 * @returns {*}
 */
function createAltitudeChart(sensorType, datasets, chartType, options) {
  let ctx = document.getElementById(`chart-${sensorType}`);
  let chart = null;

  chart = new Chart(ctx, {
    type:                chartType,
    responsive:          true,
    maintainAspectRatio: false,
    options:             {
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'second'
          }
        }],
        yAxes: [{
          position: 'left',
          id: 'y-axis-altitude',
          ticks: {
            min:         0,
            max:         35000,
//            beginAtZero: true,
            stacked:     true
          }
        }, {
          position: 'right',
          id: 'y-axis-temperature',
          ticks: {
            min:         -60,
            max:         60,
//            beginAtZero: true,
            stacked:     true
          }
        }]
      }
    }
  });

  for (const dataset of datasets) {
    chart.data.datasets.push({
      label:           dataset.label,
      backgroundColor: [dataset.backgroundColor],
      borderColor:     [dataset.borderColor]
    });
  }

  chart.update();

  return chart;
}


/**
 * Create a light chart, given its related sensor-type and datasets.
 * @param sensorType
 * @param datasets
 * @param chartType
 * @returns {*}
 */
function createLightChart(sensorType, datasets, chartType) {
  let ctx = document.getElementById(`chart-${sensorType}`);
  let chart = new Chart(ctx, {
    type:                chartType,
    responsive:          true,
    maintainAspectRatio: false,
    options:             {
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'second'
          }
        }],
        yAxes: [{
          ticks: {
            min:         0,
            max:         100,
            beginAtZero: true,
            stacked:     true
          }
        }]
      }
    }
  });

  for (const dataset of datasets) {
    chart.data.datasets.push({
      label:           dataset.label,
      backgroundColor: [dataset.backgroundColor],
      borderColor:     [dataset.borderColor]
    });
  }

  chart.update();

  return chart;
}

function onSystemInfo(data) {
  let chart = sensors.systemInfo.chart;
  chart.data.datasets[0].data[0] = data.message.cpu * 100;
  chart.data.datasets[0].data[1] = data.message.memory * 100;
  chart.data.datasets[0].data[2] = data.message.disk * 100;
  chart.update();

}

function onBattery(charge = 1.0) {
  let index = Math.round(charge * 4);
  let batteryClass = `fa-battery-${index}`;
  $('#battery').removeClass('fa-battery-0 fa-battery-1 fa-battery-2 fa-battery-3 fa-battery-4');
  $('#battery').addClass(batteryClass);

  console.log(batteryClass);
}

function onHeartbeat() {
  let el = document.getElementById('heartbeat');
  el.style.animation = 'none';
  el.offsetHeight; /* trigger reflow */
  el.style.animation = null;
}

function onMessage(data) {

  if (data && data.message) {
    if (!data.message.hasOwnProperty('time')) {
      data.message.time = Date.now();
    }
  }

  switch (data.room) {
    case '/sensor/hmc5883l': {
      pointCompass(data.message.heading);
    } break;

    case '/sensor/bme280': {
      console.log(`${data.message.temperature} C, ${data.message.humidity} RH%, ${data.message.pressure} kPa`);

      data.message.pressure = data.message.pressure / 10;
      for (const entry of ['temperature', 'humidity', 'pressure']) {
        addDataPoint('environment', entry, data.message.time, data.message[entry]);
      }

    } break;

    case '/sensor/heartbeat': {
      // .removeClass('heartbeat-pulse').addClass('heartbeat-pulse');
      // var elm = $('#heart');
      // var newone = $('#heart');
      // $('#heart').replaceWith(newone);
      onHeartbeat();
    } break;

    case '/sensor/system': {
      onSystemInfo(data);
    } break;

    case '/sensor/tsl2591': {
      for (const entry of ['ir', 'visibility']) {
        addDataPoint('light', entry, data.message.time, data.message[entry]);
      }
    } break;

    case '/sensor/gps': {
      $('#gps-coords').html(`${data.message.latitude}, ${data.message.longitude}`);

      for (const entry of ['altitude']) {
        addDataPoint('altitude', entry, data.message.time, data.message[entry]);
      }
      $('#gps-altitude').html(`${data.message.altitude}`);

    } break;

    case '/sensor/lis3dh': {
      for (const entry of ['accelX', 'accelY', 'accelZ']) {
        addDataPoint('accelerometer', entry, data.message.time, data.message[entry]);
      }
    } break;

    case '/sensor/si1145': {
      for (const entry of ['uv']) {
        addDataPoint('light', entry, data.message.time, data.message[entry]);
      }
    } break;

    case '/sensor/camera': {
      $("#camera-image").attr("src", data.message.url);
      $("#camera-image-path").text(data.message.url);
    } break;

    default: {
      if (data.room) {
        console.log(`${data.room} not captured.`);
      }
    } break;
  }
}

function onDisconnected () {
  $('#connected').removeClass('connected');
  $('#connected').removeClass('fa-thumbs-o-up');
  $('#connected').addClass('disconnected');
  $('#connected').addClass('fa-thumbs-o-down');
}

function onConnected () {
  $('#connected').removeClass('disconnected');
  $('#connected').removeClass('fa-thumbs-o-down');
  $('#connected').addClass('connected');
  $('#connected').addClass('fa-thumbs-o-up');
}

function unsubscribeChannels(client, channels) {
  // Connect to channels
  for (const channel of channels) {
    client.roomLeave(`/sensor/${channel.toLowerCase()}`, data => {
      console.log(data);
    });
  }
}

function subscribeChannels(client, channels) {
  // Connect to channels
  for (const channel of channels) {
    client.roomAdd(`/sensor/${channel.toLowerCase()}`, data => {
      console.log(data);
    });
  }
}

function connectWebsocket() {
  if (client) {
    unsubscribeChannels(client, SensorChannels);
    client.disconnect();
    client = null;
  }

  client = new ActionheroWebsocketClient;

  client.on('connected',    onConnected);
  client.on('disconnected', onDisconnected);

  client.on('error',        function(error){ console.log('error', error.stack) });
  client.on('reconnect',    function(){ console.log('reconnect') });
  client.on('reconnecting', function(){ console.log('reconnecting') });

  client.on('message',      onMessage);

  client.on('alert',        function(message){ alert( JSON.stringify(message) ) });
  client.on('api',          function(message){ alert( JSON.stringify(message) ) });

  client.on('welcome',      function(message){  });
  client.on('say',          function(message){  });

  client.connect((err, details) => {
    if (err) {
      console.log(err);
      return;
    }

    console.log(details);

    subscribeChannels(client, SensorChannels);
  });
}

function setupMap() {
return;
  let url = 'http://192.168.99.100:32772/styles/osm-bright/{z}/{x}/{y}.png';
  // latitude=43.383293, longitude=-80.310753

  let map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.XYZ({
          url: url
        })
      })
    ],
    view: new ol.View({
      projection: 'EPSG:4326',
      center: [-80.310753, 43.383293],
      zoom: 9
    })
  });
}

/**
 * Initial boot function; called at window.onload time.
 */
function boot() {
  console.log('window::onload()');

  connectWebsocket();

  sensors.environment.chart = createChart('environment', [{
    label: '°C',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(32,128,255,1)'
  }, {
    label: 'RH%',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(255,128,32, 1)'
  }, {
    label: 'kPa',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(0,255,32, 1)'
  }], 'line');

  sensors.accelerometer.chart = createAccelerometerChart('accelerometer', [{
    label: 'x',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(32,128,255,1)'
  }, {
    label: 'y',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(255,128,32, 1)'
  }, {
    label: 'z',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(0,255,32, 1)'
  }], 'line');

  sensors.light.chart = createLightChart('light', [{
    label: 'IR',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(32,128,255,1)'
  }, {
    label: 'Vis',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(255,128,32, 1)'
  }, {
    label: 'UV',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(0,255,32, 1)'
  }], 'line');

  sensors.altitude.chart =   createAltitudeChart('altitude', [{
    label: 'Altitude (m)',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(32,128,255,1)',
    fill: true,
    yAxisID: 'y-axis-altitude'
  }, {
    label: '°C (ext)',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(255,128,32, 1)',
    fill: false,
    yAxisID: 'y-axis-temperature'
  },{
    label: '°C (int)',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderColor: 'rgba(0,255,32, 1)',
    fill: false,
    yAxisID: 'y-axis-temperature'
  }], 'line');

  sensors.systemInfo.chart = createSystemInfoChart();

  $('#connected').on('click', () => {
    connectWebsocket();
  });

  setupMap();
}

function deviceOrientationHandler(tiltLR, tiltFB, dir) {
  // Rotate the disc of the compass.
  // Laat de kompas schijf draaien.
  // var compassDisc = document.getElementById("compassDiscImg");
  // compassDisc.style.webkitTransform = "rotate("+ dir +"deg)";
  // compassDisc.style.MozTransform = "rotate("+ dir +"deg)";
  // compassDisc.style.transform = "rotate("+ dir +"deg)";
  //
  let arrow = document.getElementById('compassArrowImg');
  arrow.style.webkitTransform = "rotate("+ dir +"deg)";
  arrow.style.MozTransform = "rotate("+ dir +"deg)";
  arrow.style.transform = "rotate("+ dir +"deg)";

}

document.addEventListener("DOMContentLoaded", function(event) {

  if (window.DeviceOrientationEvent) {
    window.addEventListener('deviceorientation', function(eventData) {
      // gamma: Tilting the device from left to right. Tilting the device to the right will result in a positive value.
      // gamma: Het kantelen van links naar rechts in graden. Naar rechts kantelen zal een positieve waarde geven.
      let tiltLR = eventData.gamma;

      // beta: Tilting the device from the front to the back. Tilting the device to the front will result in a positive value.
      // beta: Het kantelen van voor naar achteren in graden. Naar voren kantelen zal een positieve waarde geven.
      let tiltFB = eventData.beta;

      // alpha: The direction the compass of the device aims to in degrees.
      // alpha: De richting waarin de kompas van het apparaat heen wijst in graden.
      let dir = eventData.alpha

      // Call the function to use the data on the page.
      // Roep de functie op om de data op de pagina te gebruiken.
      deviceOrientationHandler(tiltLR, tiltFB, dir);
    }, false);
  } else {
    document.getElementById("notice").innerHTML = "Helaas. De DeviceOrientationEvent API word niet door dit toestel ondersteund."
  }
});
