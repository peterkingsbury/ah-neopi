module.exports = {
  "extends": "google",
  "rules": {
    "no-multi-spaces": 0,
    "max-len": 0,
    "comma-dangle": 0,
    "object-curly-spacing": 0,
    "arrow-parens": 0,
    "new-cap": 0,
    "key-spacing": 0,
    "require-jsdoc": 0
  },
  "parserOptions": {
    "ecmaVersion": 8,
    "sourceType": "module"
  },
  "env": { "es6": true }
};
