'use strict';
const {api, Initializer} = require('actionhero');
const Sequelize = require('sequelize');
const path = require('path');
const fs = require('fs');

module.exports = class MyInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'database';
    this.loadPriority = 1000;
    this.startPriority = 1000;
    this.stopPriority = 1000;
  }

  async loadModels(db) {
    let baseDir = path.join(__dirname, '../lib/model/');
    let files = fs.readdirSync(baseDir);
    for (const file of files) {
      let filepath = path.join(baseDir, file);
      if (fs.lstatSync(filepath).isFile()) {
        api.log(`Loading ${filepath}`, 'debug');
        db.import(path.join(__dirname, '../lib/model/', file));
      }
    }

    let models = Object.keys(db.models);

    for (const model of models) {
      await db.models[model].sync();
    }
  }

  async initialize() {
    api.log(`${this.loadPriority} ${this.name} initializing`);
    let cfg = api.config.database;
    let database = new Sequelize(cfg.db, cfg.user, cfg.pass, cfg.options);
    await database.authenticate();
    await this.loadModels(database);
    api.database = database;
  }

  async start() {}
  async stop() {}
};
