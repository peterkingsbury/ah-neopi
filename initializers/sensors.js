'use strict';
const {api, Initializer} = require('actionhero');
const { Packet, PACKET_ID } = require('../lib/packet/Packet');

module.exports = class SensorInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'sensors';
    this.loadPriority = 1200;
    this.startPriority = 1200;
    this.stopPriority = 1200;
  }

  async initializePayload() {
    api.log(`Sensor startup`);

    /*
    LIS3DH    0x18
    HMC5883L  0x1e
    BME280    0x77
    TSL2591   0x29
    DS18B20   n/a
    SI1145    0x60
    GPS       0x04
    */

    const LIS3DH   = require('../lib/sensor/LIS3DH').LIS3DH;
    const HMC5883L = require('../lib/sensor/HMC5883L').HMC5883L;
    const BME280   = require('../lib/sensor/BME280').BME280;
    const TSL2591  = require('../lib/sensor/TSL2591').TSL2591;
    const DS18B20  = require('../lib/sensor/DS18B20').DS18B20;
    const SI1145   = require('../lib/sensor/SI1145').SI1145;
    const GPS      = require('../lib/sensor/GPS').GPS;

    api.sensors = {
      Accelerometer: new LIS3DH({db: api.database, cache: api.cache, packetId: PACKET_ID.ACCELEROMETER}),
      Compass:       new HMC5883L({db: api.database, cache: api.cache, packetId: PACKET_ID.COMPASS}),
      Environment:   new BME280({db: api.database, cache: api.cache, packetId: PACKET_ID.ENVIRONMENT}),
      Luminosity:    new TSL2591({db: api.database, cache: api.cache, packetId: PACKET_ID.LUMINOSITY}),
      Temperature:   new DS18B20({db: api.database, cache: api.cache, packetId: PACKET_ID.TEMPERATURE}),
      Ultraviolet:   new SI1145({db: api.database, cache: api.cache, packetId: PACKET_ID.ULTRAVIOLET}),
      GPS:           new GPS({db: api.database, cache: api.cache, packetId: PACKET_ID.GPS})
    };

    for (const key of Object.keys(api.sensors)) {
      let sensor = api.sensors[key];
      sensor.events.on('reading', async reading => {
        // console.log(`---------------------------------------------`);
        // console.log(JSON.stringify(reading, null, 2));
        // console.log(`---------------------------------------------`);
        try {
          await api.tasks.enqueue('transmitRadioData', reading, 'default');
        } catch (e) {
          api.log(`Error enqueueing task 'transmitRadioData'`, 'error', e);
        }
      });
    }

    api.sensors = Object.assign(api.sensors, {
      LIS3DH:   api.sensors.Accelerometer,
      HMC5883L: api.sensors.Compass,
      BME280:   api.sensors.Environment,
      TSL2591:  api.sensors.Luminosity,
      DS18B20:  api.sensors.Temperature,
      SI1145:   api.sensors.Ultraviolet,
      GPS:      api.sensors.GPS
    });

    api.readings = {
      ir:            0,
      visibility:    0,
      uv:            0,
      heading:       0,
      temperature:   0,
      humidity:      0,
      pressure:      0,
      altitude:      0,
      accelX:        0,
      accelY:        0,
      accelZ:        0,
      latitude:      0,
      longitude:     0,
      altitude_gps:  0,
      external_temp: 0
    };

    let sensorKeys = Object.keys(api.sensors);
    for (const key of sensorKeys) {
      try {
        await api.sensors[key]._init();
        api.log(`Initialized sensor ${key}`, 'notice');
      } catch (e) {
        api.log(`Unable to initialize sensor '${key}'`, 'error');
      }
    }
  }

  async initializeMissionControl() {
    let sensors = [
      'LIS3DH',
      'HMC5883L',
      'BME280',
      'TSL2591',
      'SI1145',
      'DS18B20',
      'GPS',
      'heartbeat',
      'system',
      'camera'
    ];

    for (const sensor of sensors) {
      try {
        let roomName = `/sensor/${sensor.toLowerCase()}`;
        api.log(`Adding channel ${roomName}`);
        await api.chatRoom.add(roomName);
      } catch (e) {
        if (e.message !== 'room exists') {
          throw e;
        }
      }
    }
  }

  async initialize() {
    api.log(`${this.loadPriority} ${this.name} initializing`);
    api['sensors'] = {};

    switch (api.mode) {
      case 'PAYLOAD': {
        await this.initializePayload();
      } break;

      case 'MISSIONCONTROL': {
        await this.initializeMissionControl();
      } break;
    }
  }

  async start() {}
  async stop() {
    for (const key of Object.keys(api.sensors)) {
      delete api.sensors[key];
    }
  }
};
