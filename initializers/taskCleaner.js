'use strict';
const { api, Initializer } = require('actionhero');
const { mapSeries } = require('bluebird');
const TASK_CHECK_INTERVAL = 60000;
const MAX_WORKER_AGE = 10000;

module.exports = class TaskCleanerInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'taskCleaner';
    this.loadPriority = 1000;
    this.startPriority = 1000;
    this.stopPriority = 1000;
  }

  async initialize() {}

  async start() {
    api.log(`Removing stuck workers older than ${MAX_WORKER_AGE} ms`, 'notice', {});

    let result = await api.resque.queue.cleanOldWorkers(MAX_WORKER_AGE);

    if (Object.keys(result).length > 0) {
      api.log(`Removed stuck workers with errors`, 'error', result);
    }

    if (api.config.tasks.scheduler) {
      api.log(`Starting interval timer (${TASK_CHECK_INTERVAL / 1000}s) to clean up old tasks`);
      setInterval(async () => {
        try {
          // api.log(`Checking for stuck tasks...`, 'info');
          let failedTasks = await api.tasks.failed(0, -1);
          await mapSeries(failedTasks, async (task) => {
            try {
              api.log('Removed failed task', 'info', task);
              await api.tasks.removeFailed(task);
            } catch (e) {
              api.log(this.name, 'error', e);
            }
          });

          await mapSeries(Object.keys(api.tasks.tasks), async (taskName) => {
            try {
              let task = api.tasks.tasks[taskName];
              if (task.frequency === 0) {
                return;
              }

              let timestamps = await api.tasks.scheduledAt(task.queue, taskName, {});
              await mapSeries(timestamps || [], async (entry) => {
                let timestamp = +entry;
                let humanReadable = (new Date(timestamp * 1000));
                if (timestamp > 0 && timestamp < (Date.now() / 1000) - 300) {
                  api.log(`Found stuck task`, 'warning', task);
                  try {
                    await api.tasks.delDelayed(task.queue, taskName, {});
                    api.log(`Cleared stuck task ${taskName}, stuck since ${humanReadable}`, 'info', task);
                    await api.tasks.enqueueRecurrentTask(taskName);
                  } catch (e) {
                    api.log(`Error clearing stuck task ${taskName}`, 'error', e);
                  }
                }
              });
            } catch (e) {
              api.log(this.name, 'error', e);
            }
          });
        } catch (e) {
          api.log(this.name, 'error', e);
        }
      }, TASK_CHECK_INTERVAL).unref();
    }
  }

  async stop () {}
};
