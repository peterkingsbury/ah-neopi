'use strict';

const { api, Initializer } = require('actionhero');
const Armature = require('../lib/armature/Armature').Armature;
const Program = require('../lib/armature/Program').Program;

module.exports = class ArmatureInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'armature';
    this.loadPriority = 1300;
    this.startPriority = 1300;
    this.stopPriority = 1300;
  }

  async initialize() {
    api.log(`${this.loadPriority} ${this.name} initializing`);

    try {
      if (api.mode === 'PAYLOAD') {
        api.log(`Armature startup`);
        api.armature = new Armature({
          panPin:         12,
          tiltPin:        13
        });

        await api.armature._init();
      }
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async start() {}
  async stop() {}
};
