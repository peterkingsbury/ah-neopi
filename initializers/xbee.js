'use strict';

const {api, Initializer} = require('actionhero');
const XBee = require('../lib/xbee/XBee').XBee;
const { Packet } = require('../lib/packet/Packet');

module.exports = class XBeeInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'xbee';
    this.loadPriority = 11000;
    this.startPriority = 11000;
    this.stopPriority = 11000;
  }

  async initialize() {
    if (!api.mode) {
      return;
    }

    try {
      api.log(`${this.loadPriority} ${this.name} initializing`);

      api.xbee = new XBee(api.config.xbee.device, api.config.xbee.baudRate);

      await api.xbee.initialize();
      await api.xbee.run(api.config.xbee.boot);

      api.xbee.events.on('data', async data => {
        // console.log(`---------------------------------------------`);
        // // console.log(JSON.stringify(data, null, 2));
        // console.log(data);
        // console.log(`---------------------------------------------`);
        try {
          let packet = new Packet();
          let radioData = packet.deserialize(data);
          // api.log(this.name, 'notice', radioData);
          await api.tasks.enqueue('receiveRadioData', radioData, 'default');
        } catch (e) {
          api.log(`Error enqueueing task 'receiveRadioData'`, 'error', e);
        }
      });
    } catch (e) {
      api.log(this.name, 'error', e);
      throw e;
    }
  }

  async start() {
  }
  async stop() {
    delete api.xbee;
  }
};
