'use strict';
const { api, Task } = require('actionhero');

module.exports = class ReadLuminosityTask extends Task {
  constructor() {
    super();
    this.name = 'readLuminosity';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 5000 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      let start = Date.now();

      let { visibility, ir } = await api.sensors.Luminosity.readLuminosity();
      await api.sensors.Luminosity.store({
        visibility,
        ir
      });

      Object.assign(api.readings, {
        visibility,
        ir
      });

      // api.log(`Read Luminosity (${Date.now() - start} ms)`);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
