'use strict';
const { api, Task } = require('actionhero');

module.exports = class ReadTemperatureTask extends Task {
  constructor() {
    super();
    this.name = 'readTemperature';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 5000 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      let start = Date.now();
      let { temperature } = await api.sensors.Temperature.read();
      await api.sensors.Temperature.store({ temperature });
      Object.assign(api.readings, {
        external_temp: temperature
      });
      // api.log(`Read Temperature (${Date.now() - start} ms)`);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
