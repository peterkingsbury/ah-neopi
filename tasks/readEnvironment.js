'use strict';
const { api, Task } = require('actionhero');

module.exports = class ReadEnvironmentTask extends Task {
  constructor() {
    super();
    this.name = 'readEnvironment';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 5000 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      // let start = Date.now();
      let env = await api.sensors.Environment.read();
      let data = {
        temperature: env.temperature_C,
        humidity: env.humidity,
        pressure: env.pressure_hPa,
        altitude: env.altitude
      };

      await api.sensors.Environment.store(data);
      Object.assign(api.readings, data);
      // api.log(`Read Environment (${Date.now() - start} ms)`);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
