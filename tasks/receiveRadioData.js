'use strict';
const { api, Task } = require('actionhero');
const { Packet, PACKET_ID } = require('../lib/packet/Packet');
const { ImageProcessor } = require('../lib/image/ImageProcessor');
const Moment   = require('moment');

module.exports = class ReceiveRadioDataTask extends Task {
  constructor() {
    super();
    this.name = 'receiveRadioData';
    this.description = 'an actionhero task';
    this.frequency = 0;
    this.queue = 'default';
    this.middleware = [];
    this.images = {};
  }

  async storeImagePacket(packet) {
    if (!this.images.hasOwnProperty(packet.data.sequence)) {
      this.images[packet.data.sequence] = [];
    }
    this.images[packet.data.sequence][packet.data.index] = packet.data.data;
    let chunks = this.images[packet.data.sequence].reduce((acc, cv)=>(cv)?acc + 1 : acc, 0);
    if (chunks === packet.data.count) {
      let now = Moment().format('YYYYMMDD-HHmmss.SSS');
      let filename = `image-${now}.png`;
      let filepath = `./public/images/${filename}`;
      api.log(`Saving completed image at ${filepath}`);
      let buffers = this.images[packet.data.sequence].map(e => { return Buffer.from(e.data) });
      await ImageProcessor.buffersToImage(buffers, filepath);
      delete this.images[packet.data.sequence];

      let room = `/sensor/camera`;
      await api.chatRoom.broadcast({
        room: api.config.xbee.name
      }, room, {
        url: filepath
      });
    }
  }

  async run(data) {
    try {
      if (!data) return;
      if (!data.hasOwnProperty('packetId')) return;

      switch (data.packetId) {
        case PACKET_ID.IMAGECHUNK: {
          await this.storeImagePacket(data);
          return;
        } break;
        default: {
          // do nothing
        } break;
      }

      let room = `/sensor/${PACKET_ID[data.packetId]}`;
      api.log(`${room}:`, 'notice', data);

      await api.chatRoom.broadcast({
        room: api.config.xbee.name
      }, room, data.data);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
