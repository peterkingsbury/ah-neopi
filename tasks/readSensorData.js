'use strict';
const { api, Task } = require('actionhero');
const Util = require('../lib/util/Util');
const ACCEPTABLE_DISTANCE_THRESHOLD = 1000;
const Home = {
  latitude: 43.383244,
  longitude: -80.310722
};
let lastGps = Home;

module.exports = class ReadSensorDataTask extends Task {
  constructor() {
    super();
    this.name = 'readSensorData';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 1000 : 0;
    this.queue = 'default';
    this.middleware = [];
  }

  async readAccelerometer() {
    try {
      let accel = await api.sensors.Accelerometer.read();

      let data = {
        accelX: accel.x,
        accelY: accel.y,
        accelZ: accel.z
      };

      await api.sensors.Accelerometer.store(data);
      Object.assign(api.readings, data);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async readCompass() {
    try {
      let heading = await api.sensors.Compass.readHeading();
      let raw = await api.sensors.Compass.readRawValues();

      await api.sensors.Compass.store({
        heading,
        x: raw.x,
        y: raw.y,
        z: raw.z
      });

      Object.assign(api.readings, {
        heading,
        compass_x: raw.x,
        compass_y: raw.y,
        compass_z: raw.z
      });
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async readEnvironment() {
    try {
      let env = await api.sensors.Environment.read();
      let data = {
        temperature: env.temperature_C,
        humidity: env.humidity,
        pressure: env.pressure_hPa,
        altitude: env.altitude
      };

      await api.sensors.Environment.store(data);
      Object.assign(api.readings, data);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async readGPS() {
    try {
      let gps = await api.sensors.GPS.read();
      await api.sensors.GPS.store(gps);
      api.readings = Object.assign(api.readings, gps);
      if (lastGps === null) {
        lastGps = gps;
      } else {
        let distanceFromHome = Util.geoDistance(Home.latitude, Home.longitude, gps.latitude, gps.longitude);
        distanceFromHome = Math.round(distanceFromHome * 100) / 100;
        if (distanceFromHome > ACCEPTABLE_DISTANCE_THRESHOLD) {
          return;
        } else {
          lastGps = gps;
        }
      }
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async readLuminosity() {
    try {
      let { visibility, ir } = await api.sensors.Luminosity.readLuminosity();
      await api.sensors.Luminosity.store({
        visibility,
        ir
      });

      Object.assign(api.readings, {
        visibility,
        ir
      });
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async readTemperature() {
    try {
      let { temperature } = await api.sensors.Temperature.read();
      await api.sensors.Temperature.store({ temperature });
      Object.assign(api.readings, {
        external_temp: temperature
      });
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async readUV() {
    try {
      let readings = {
        uv: await api.sensors.Ultraviolet.readUV(),
        visible: await api.sensors.Ultraviolet.readVisible(),
        ir: await api.sensors.Ultraviolet.readIR(),
        prox: await api.sensors.Ultraviolet.readProx(),
      };

      await api.sensors.Ultraviolet.store(readings);
      Object.assign(api.readings, {
        readings
      });
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }

  async run() {
    try {
      await this.readAccelerometer();
      await this.readCompass();
      await this.readEnvironment();
      await this.readGPS();
      await this.readLuminosity();
      await this.readTemperature();
      await this.readUV();
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
