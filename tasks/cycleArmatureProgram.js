'use strict';

const Program = require('../lib/armature/Program').Program;
const { ImageProcessor } = require('../lib/image/ImageProcessor');
const { Camera } = require('../lib/camera/Camera').Camera;
const { PACKET_ID } = require('../lib/packet/Packet');
const gm = require('gm');
const Bluebird = require('bluebird');
const fs = require('fs');
const exec = Bluebird.promisify(require('child_process').exec);

Bluebird.promisifyAll(gm.prototype);
Bluebird.promisifyAll(fs);

const programs = [
//  new Program('zero',          0.5, 0.5),
//  new Program('left_top',      1.0, 0.0),
    new Program('left_center',   1.0, 0.5),
//  new Program('left_bottom',   1.0, 1.0),
    new Program('center',        0.5, 0.5),
//  new Program('center_bottom', 0.5, 1.0),
//  new Program('right_top',     0.0, 0.0),
    new Program('right_center',  0.0, 0.5),
//  new Program('right_bottom',  0.0, 1.0),
    new Program('center_top',    0.5, 0.0),
    new Program('center_bottom', 0.5, 1.0)
];

let currentProgram = 0;
let currentImageSequence = 0;

const {api, Task} = require('actionhero');

module.exports = class CycleArmatureTask extends Task {
  constructor() {
    super();
    this.name = 'cycleArmatureProgram';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 10000 : 0;
    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    api.log(`running armature cycle`);
    try {
      // Get the next program
      let program = programs[currentProgram];
      // Run the program and take a photo
      await api.armature.runProgram(program);

      // let imageFile = await api.camera.capture();
      await api.camera.capture({
        rotation: 90
      });

      let sendLength = await api.redis.clients.client.llen(`resque:queue:image`);
      if (sendLength === 0) {
        // Take a second, smaller photo
        let imageFile = await api.camera.capture({
          width: 320,
          height: 240,
          rotation: 90,
          encoding: 'png'
        });

        // await gm(imageFile).mapAsync(api.config.camera.vgaPalette);

        await exec(`convert ${imageFile} -map ${api.config.camera.vgaPalette} ${imageFile}`);

        let buffers = await ImageProcessor.imageToBuffers(imageFile, 100);

        for (let i = 0; i < buffers.length; i++) {
          // await api.tasks.enqueue();
          let packet = {
            sequence: currentImageSequence,
            count: buffers.length,
            index: i,
            data: buffers[i].toString('hex')
          };

          await api.tasks.enqueue('transmitRadioData', {
            packetId: PACKET_ID.IMAGECHUNK,
            data: packet
          }, 'image');
        }

        await fs.unlinkAsync(imageFile);

        currentImageSequence++;
      }

      // Increment and wrap currentProgram
      currentProgram++;
      if (currentProgram >= programs.length) {
        currentProgram = 0;
      }
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
