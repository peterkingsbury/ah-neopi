import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

# Setup the Pan
pinPan = 12
GPIO.setup(pinPan, GPIO.OUT)
pan = GPIO.PWM(pinPan, 20)
pan.start(5)
pan.ChangeDutyCycle(7.5)

# Setup the Tilt
pinTilt = 13
GPIO.setup(pinTilt, GPIO.OUT)
tilt = GPIO.PWM(pinTilt, 20)
tilt.start(5)
tilt.ChangeDutyCycle(7.5)

GPIO.cleanup(pinPan)
GPIO.cleanup(pinTilt)
