i2c_addr = 0x4
import smbus as smbus
i2c = smbus.SMBus(1)

def i2cwrite(str):
  i2c.write_byte(i2c_addr, 0x02)
  for i in map(ord, list(str)):
    i2c.write_byte(i2c_addr, i)
  i2c.write_byte(i2c_addr, 0x03)

i2cwrite("Hello, world!")


# import smbus
# import time
#
# bus = smbus.SMBus(1)
#
# address = 0x04
#
# def writeNumber(value):
#   bus.write_byte(address, value)
#   return -1
#
# def readNumber():
#   number = bus.read_byte(address)
#   return number
#
# var = 0
# while True:
#   writeNumber(var)
#   print "RPI> ", var
#   time.sleep(.5)
#
#   number = readNumber()
#   print "ARD>  ", number
#   print
#
#   var = var + 1
#   if var > 255:
#     var = 0
