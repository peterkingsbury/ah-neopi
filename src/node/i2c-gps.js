'use strict';

const Promise = require('bluebird');
const i2c = require('i2c');
const address = 0x04;

// wire.writeByte(1, function(err) {
//   console.log(err);
// });

// wire.scan(function(err, data) {
//   console.log(err);
//   console.log(data);
//   wire.readBytes(0x1, 32, function(err, res) {
//     console.log(err);
//     console.log(res);
//   });
// });

// const Promise = require('bluebird');

function run() {
  let wire = Promise.promisifyAll(new i2c(address, {device: `/dev/i2c-1`}));
  return wire.writeByteAsync(1).then(() => {
    return wire.readBytesAsync(1, 32).then(result => {
      console.log(result.toString());
    });
  });

  // return new Promise((resolve, reject) => {
  //   wire.writeByte(1, err => {
  //     if (err) {
  //       return reject(err);
  //     }
  //     wire.readBytes(1, 32, (err, res) => {
  //       if (err) {
  //         return reject(err);
  //       }
  //
  //       console.log(res.toString());
  //       resolve(res);
  //     });
  //   });
  // });


}

setInterval(() => {
  run().catch(err => {
    // console.log(err.stack);
  });
}, 250);
