'use strict';

const xbeePromise = require('xbee-promise');
const Promise = require('bluebird');

const addr64 = '0013a200415b3c95';

let xbee = xbeePromise({
  serialport: '/dev/ttyAMA0',
  serialPortOptions: {
    baudrate: 115200
  },
  module: '802.15.4',
  debug: true
});

xbee.remoteTransmit({
  destination64: addr64,
  data: 'hello from blue promise'
}).then(response => {
  console.log(response);
}).catch(err => {
  console.trace(err);
}).fin(() => {
  xbee.close();
});
