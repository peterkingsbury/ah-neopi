'use strict';

const SPI = require('spi-device');

class SPIDevice {
  constructor(bus = 0, device = 0, speedDivider = 1) {
    this._bus = bus;
    this._device = device;
    this._speedDivider = speedDivider;
    this._maxSpeedHz = 0;
    this._spi = 0;
  }

  initialize() {
    return new Promise((resolve, reject) => {
      this._spi = SPI.open(this._bus, this._device, err => {
        if (err) {
          return reject(err);
        }

        // Get the device max speed
        this._spi.getOptions((err, options) => {
          if (err) {
            return reject(err);
          }

          console.log(options);

          this._maxSpeedHz = options.maxSpeedHz;
          resolve();
        });
      });
      return null;
    });
  }

  get speed() {
    return this._maxSpeedHz / this._speedDivider;
  }

  send(msg) {
    return new Promise((resolve, reject) => {

      let send = null;

      if (typeof msg === 'string') {
        send = Buffer.from(msg, 'binary');
      } else if (msg instanceof Buffer) {
        send = msg;
      } else {
        return reject(new Error(`invalid message; must be a Buffer`));
      }

      let length = msg.length;

      this._spi.transfer([{
        sendBuffer: send,
        byteLength: length,
        speedHz: this.speed
      }], (err, result) => {
        if (err) {
          return reject(err);
        }

        resolve(result);
      });
    });
  }

  _transfer(msg) {

  }
}

module.exports.SPIDevice = SPIDevice;

function test() {
  let a = new SPIDevice();
  a.initialize().then(() => {
    return a.send('ATSDhelloworld').then(result => {
      for (const message of result) {
        // console.log(Buffer.from(message.sendBuffer, 'binary'));
        let buffer = message.sendBuffer;
        console.log(`ascii:   ${buffer.toString('ascii')}`);
        console.log(`utf16le: ${buffer.toString('utf16le')}`);
        console.log(`base64:  ${buffer.toString('base64')}`);
        console.log(`latin1:  ${buffer.toString('latin1')}`);
        console.log(`binary:  ${buffer.toString('binary')}`);

      }
    });
  }).catch(err => {
    console.trace(err);
  });
}

test();
