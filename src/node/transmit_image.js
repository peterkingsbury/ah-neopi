'use strict';

const Bluebird = require('bluebird');
const fs = Bluebird.promisifyAll(require('fs'));
const BufferPack = require('bufferpack');

const MAX_PACKET_SIZE = 260;

const PACKET_HEADER = {
  SequenceID: 0, // the sequence in which this packet belongs
  PacketIndex: 0, // the index of the packet in its sequence
  TotalPackets: 0, // the total packets in this sequence
  PayloadType: 0, // the type of data the sequence represents
  PayloadFormat: 0, // the format the data should be converted from
};

let testFile = `/Users/peter/Development/NeoPi/ah-neopi/data/8bit.jpg`;

function deconstruct(filename) {
  return fs.readFileAsync(filename, {}).then(output => {
    let totalBufferLength = output.length;
    console.log(`Total length: ${totalBufferLength} bytes`);

  });
}

function reconstruct(sequence) {

}

deconstruct(testFile).then(result => {

}).catch(err => {
  console.trace(err);
});
