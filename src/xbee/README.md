

## Definitions

<dl>
  <dt>Packet Index<dt>
  <dd>A 0-based unique number which allows a collection of Packets to be grouped together.</dd>

  <dt>Packet Sequence</dt>
  <dd>A 0-based number for a set of Packets which form a larger Payload. Once a Packet Group receives all packets in a sequence, it attempts to resequence them. If a packet sequence becomes too old, it flushes the data to disk.</dd>

  <dt>Data Reassembler</dt>
  <dd>A container for groups of similarly-indexed packets. </dd>

  <dt>Data Disassembler</dt>
  <dd>A controller which intakes a set of data, and converts it into a sequence of packets.</dd>

  <dt></dt>
  <dd></dd>

  <dt></dt>
  <dd></dd>

  <dt></dt>
  <dd></dd>

  <dt></dt>
  <dd></dd>

  <dt></dt>
  <dd></dd>

</dl>
