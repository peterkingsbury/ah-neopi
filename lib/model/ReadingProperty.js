'use strict';

// const { api } = require('actionhero');

module.exports = function(sequelize, DataTypes) {
  // api.log(`Indexes turned off for reading_property`, 'notice');
  return sequelize.define('reading_property', {
    value: {
      type: DataTypes.DOUBLE,
      field: 'value'
    },
    reading_id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'reading',
        key: 'id'
      }
    },
    property_id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'property',
        key: 'id'
      }
    },
  }, {
    freezeTableName: true,
    timestamps: false,
    indexes: [
      {
        unique: true,
        method: 'BTREE',
        fields: ['reading_id', 'property_id']
      }
    ]
  });
};
