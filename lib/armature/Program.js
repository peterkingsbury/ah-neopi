'use strict';

class Program {
  constructor(name, pan, tilt) {
    this._name = name;
    this._pan = pan;
    this._tilt = tilt;
  }

  get name() {
    return this._name;
  }

  get pan() {
    return this._pan;
  }

  get tilt() {
    return this._tilt;
  }
}

module.exports.Program = Program;
