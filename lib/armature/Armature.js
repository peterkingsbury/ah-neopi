'use strict';

const SG90 = require('../servo/SG90').SG90;
const SleepMS = 10;
const Util = require('../util/Util');

class Armature {
  constructor(options) {
    this._pan = new SG90({
      pin: options.panPin
    });

    this._tilt = new SG90({
      pin: options.tiltPin
    });
  }

  async _init() {
    await this._pan._init();
    await this._tilt._init();
  }

  _shutdown() {
    this._pan._shutdown();
    this._tilt._shutdown();
  }

  runProgram_v1(program, captureImage = true) {
   /* return new Promise((resolve, reject) => {
      this._pan.move(program.pan);
      this._tilt.move(program.tilt);

      if (captureImage) {
        setTimeout(() => {
          this._camera.capture().then(imageFileName => {
            resolve(imageFileName);
          });
        }, SleepMS);
      } else {
        resolve();
      }
    });*/
  }

  async runProgram(program) {
    this._pan.move(program.pan);
    this._tilt.move(program.tilt);
    await Util.delay(SleepMS);
  }
}

module.exports.Armature = Armature;
