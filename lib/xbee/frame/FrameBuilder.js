'use strict';

const ByteBuffer = require('bytebuffer');

const FRAME_TYPE = {
  TRANSMIT_REQUEST: 0x10
};

const START_DELIMITER = 0x7e;
const MAX_LENGTH = 256;

/**
 * Class used to generate API frames for sending over-the-air.
 */
class FrameBuilder {
  /**
   * ctor. Does nothing!
   */
  constructor() {}

  /**
   * Given the type, build an API frame for sending.
   * @param {Number} type
   * @param {Object} data
   * @return {*}
   */
  build(type, data) {
    switch (type) {
      case FRAME_TYPE.TRANSMIT_REQUEST: {
        return this.buildTransmitRequest(data);
      } break;
      default: {
        throw new Error(`Unsupported frame type ${type}`);
      } break;
    }
  }

  checksum(buffer, from, to) {
    // See http://knowledge.digi.com/articles/Knowledge_Base_Article/Calculating-the-Checksum-of-an-API-Packet
    let check = 0;

    for (let i = from; i <= to; i++) {
      check += buffer[i];
    }

    return 0xff - (check & 0xff);
  }

  buildTransmitRequest(data) {
    if (data.data.length > MAX_LENGTH) {
      throw new Error(`data.data (length ${data.data.length}) exceeded maximum length of ${MAX_LENGTH}`);
    }

    let overhead = 18;
    let offset = 0;
    let totalLength = data.data.length + overhead;
    let buffer = new Buffer(totalLength);
// <Buffer 7e 00 15 90 00 13 a2 00 41 5b 3c a7 ff fe c1 08 7f c0 00 00 40 e0 00 00 16>
    // Start delimiter
    buffer.writeUInt8(START_DELIMITER, offset);
    offset += 1;

    // Length
    buffer.writeUInt16BE(totalLength - 4, offset);
    offset += 2;

    // Frame type
    buffer.writeUInt8(FRAME_TYPE.TRANSMIT_REQUEST, offset);
    offset += 1;

    // Frame ID
    buffer.writeUInt8(data.frameId || 0, offset);
    offset += 1;

    // 64-bit dest. address
    let destination64 = ByteBuffer.fromHex(data.destination64);
    destination64.buffer.copy(buffer, offset);
    offset += destination64.buffer.length;

    // 16-bit dest. address
    let destination16 = ByteBuffer.fromHex(data.destination16);
    destination16.buffer.copy(buffer, offset);
    offset += destination16.buffer.length;

    // Broadcast radius
    buffer.writeUInt8(0, offset);
    offset += 1;

    // Options
    buffer.writeUInt8(0, offset);
    offset += 1;

    // RF Data
    data.data.copy(buffer, offset);
    offset += data.data.length;

    // Checksum
    buffer.writeUInt8(this.checksum(buffer, 3, buffer.length - 1), offset);
    offset += 1;

    // Final sanity check
    if (offset !== buffer.length) {
      throw new Error(`Calculated buffer length mismatch; ${offset} should be ${buffer.length}`);
    }

    return buffer;
  }
}

module.exports = {
  FrameBuilder,
  FRAME_TYPE
};
