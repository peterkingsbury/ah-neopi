'use strict';

const Buffer = require('buffer');
const BufferPack = require('bufferpack');

class Packet {
  constructor(index, sequence, totalPackets, payloadType, payloadFormat, data) {
    //if (!index)         { throw new Error(`invalid index ${index}`); }
    //if (!sequence)      { throw new Error(`invalid sequence ${sequence}`); }
    //if (!totalPackets)  { throw new Error(`invalid totalPackets ${totalPackets}`); }
    //if (!payloadType)   { throw new Error(`invalid payloadType ${payloadType}`); }
    //if (!payloadFormat) { throw new Error(`invalid payloadFormat ${payloadFormat}`); }
    //if (!data)          { throw new Error(`invalid data ${data}`); }

    this._index = index;
    this._sequence = sequence;
    this._totalPackets = totalPackets;
    this._payloadType = payloadType;
    this._payloadFormat = payloadFormat;
    this._buffer = data;
    this._isTransmitted = false;
    this._createdAt = new Date();
  }

  pack() {}

  unpack() {}

}

module.exports.Packet = Packet;
