'use strict';

const Packet = require('./Packet').Packet;
const PacketSequence = require('./PacketSequence').PacketSequence;
// const Buffer = require('buffer');
const BufferPack = require('bufferpack');

const INPUT_TYPE = {
  Compass: 0x0,
  Environment: 0x1,
  Luminosity: 0x2,
  Ultraviolet: 0x3,
  GPS: 0x4,
  Image: 0x5
};

const OUTPUT_FORMAT = {
  NUMBER:    0x0,
  STRING:    0x1,
  JSON:      0x2,
  DELIMITED: 0x3,
  JPEG:      0x4
};

const MTU = 100;
const DEFAULT_MTU_SAFE_BUFFER = 0;

const DEFAULT_DELIMITER = '|';
const DEFAULT_PACKET_INDEX_BYTES = 4;
const DEFAULT_PACKET_SEQUENCE_BYTES = 2;
const DEFAULT_PACKET_TOTAL_BYTES = 2;
const DEFAULT_PACKET_TYPE_BYTES = 1;
const DEFAULT_PACKET_FORMAT_BYTES = 1;

/**
 * Could be called a PacketSequenceFactory; intakes a data object, and converts it into a packet sequence object.
 */
class DataDisassembler {
  constructor(options = {}) {
    this._delimiter           = options.delimiter           || DEFAULT_DELIMITER;
    this._packetIndexBytes    = options.packetIndexBytes    || DEFAULT_PACKET_INDEX_BYTES;
    this._packetSequenceBytes = options.packetSequenceBytes || DEFAULT_PACKET_SEQUENCE_BYTES;
    this._packetTotalBytes    = options.packetTotalBytes    || DEFAULT_PACKET_TOTAL_BYTES;
    this._packetTypeBytes     = options.packetTypeBytes     || DEFAULT_PACKET_TYPE_BYTES;
    this._packetFormatBytes   = options.packetFormatBytes   || DEFAULT_PACKET_FORMAT_BYTES;
    this._mtuSafeBuffer       = options.mtuSafeBuffer       || DEFAULT_MTU_SAFE_BUFFER;

    this._headerSize =
      this._packetIndexBytes +
      this._packetSequenceBytes +
      this._packetTotalBytes +
      this._packetTypeBytes +
      this._packetFormatBytes;

    this._bodySize = MTU - (this._headerSize + this._mtuSafeBuffer);

    this._headerFormat = this._buildHeaderFormat();

    this._packetIndex = 0;
  }

  setDelimiter(delimiter) {
    this._delimiter = delimiter;
  }

  /**
   * Intake data, and return a PacketSequence
   * @param data
   * @param payloadType
   * @param outputFormat
   */
  disassemble(data, payloadType, outputFormat) {
    if (!data) {
      throw new Error('invalid data provided');
    }

    if (typeof data !== 'object') {
      throw new Error('invalid data type');
    }

    let startTime = Date.now();
    let sequence = 0;
    let convertedData = null;
    let convertedDataLength = 0;

    // Convert the incoming data based on the desired output format
    switch (outputFormat) {
      case OUTPUT_FORMAT.DELIMITED: {
        convertedData = Object.keys(data).sort().map(key => data[key]);
        if (convertedData.length <= 0) {
          throw new Error('empty object provided');
        }

        convertedDataLength = convertedData.length - 1; // One delimiter per property, minus one

        convertedData.forEach(el => {
          switch (typeof el) {
            case 'number':
              convertedDataLength += 4;
              break;
            // We can't support variable-length strings without some kind of witchcraft.
            // case 'string':
            //  convertedDataLength += el.length;
            //  break;
            default:
              throw new Error(`data contains invalid property of type ${typeof el}`);
          }
        });
      } break;

      case OUTPUT_FORMAT.JSON: {
        convertedData = JSON.stringify(data);
        convertedDataLength = convertedData.length;
      } break;

      default: {

      } break;
    }

    if (!convertedData) {
      throw new Error(`invalid data conversion format ${outputFormat}`);
    }

    let totalPackets = Math.ceil(convertedDataLength / this._bodySize);

    console.log(`creating ${totalPackets} packets to send data ${convertedData}`);

    let packetSequence = new PacketSequence({
      sequenceLength: totalPackets
    });

    // Create the data buffer
    let dataBuffer = Buffer.alloc(convertedDataLength);
    let position = 0;
    while (convertedData.length) {
      let item = convertedData.shift();
      if (item) {
        BufferPack.packTo('f', dataBuffer, position, [item]);
        position += 4;
      }

      if (convertedData.length) {
        BufferPack.packTo('c', dataBuffer, position, [this._delimiter]);
        position += 1;
      }
    }

    let done = false;
    position = 0;
    let totalBytes = 0;
    let packetBufferLength = this._headerSize + this._bodySize;
    while (!done) {
      let packetBuffer = Buffer.alloc(packetBufferLength);
      BufferPack.packTo(this._headerFormat, packetBuffer, 0, [
        this._packetIndex,
        sequence,
        totalPackets,
        payloadType,
        outputFormat
      ]);

      let start = position;
      let end = position + this._bodySize;
      position += this._bodySize;

      dataBuffer.copy(packetBuffer, this._headerSize, start, end);

      packetSequence.addPacket(sequence, new Packet(
        this._packetIndex,  // index
        sequence,           // packetSequence
        totalPackets,       // totalPackets
        payloadType,        // payloadType
        outputFormat,       // payloadFormat
        packetBuffer        // all the header info, and some of the data
      ));

      sequence++;

      totalBytes += end - start;
      if (totalBytes >= convertedDataLength) {
        done = true;
      }
    }

    // console.log(packetSequence);

    let endTime = Date.now();

    console.log(`took ${endTime - startTime} ms`);

    this._packetIndex++;
  }

  _buildHeaderFormat() {
    let headerFormat = '';

    let formats = {
      '1': 'B',
      '2': 'H',
      '4': 'L',
      '8': 'd'
    };

    headerFormat += formats[this._packetIndexBytes];
    headerFormat += formats[this._packetSequenceBytes];
    headerFormat += formats[this._packetTotalBytes];
    headerFormat += formats[this._packetTypeBytes];
    headerFormat += formats[this._packetFormatBytes];

    return headerFormat;
  }
}

module.exports = {
  DataDisassembler,
  INPUT_TYPE,
  OUTPUT_FORMAT
};

/*
// TODO: Remove this code!
let dd = new DataDisassembler();
let data = {
  a: 1, c: 3, b: 2, d: 4,
  a1: 1, c1: 3, b1: 2, d1: 4,
  a2: 1, c2: 3, b2: 2, d2: 4,
  a3: 1, c3: 3, b3: 2, d3: 4,
  a4: 1, c4: 3, b4: 2, d4: 4,
  a5: 1, c5: 3, b5: 2, d5: 4,
  a6: 1, c6: 3, b6: 2, d6: 4
};
console.log(data);
dd.disassemble(data, 0, 3);
*/
