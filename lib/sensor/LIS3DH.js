'use strict';

const Sensor = require('../core/Sensor').Sensor;
const i2c = require('i2c');
const Bluebird = require('bluebird');

const WHO_AM_I     = 0x0f;
const CTRL_REG1    = 0x20;
const CTRL_REG4    = 0x23;
// const OUT_ADC1_L   = 0x08;
// const OUT_ADC1_H   = 0x09;
// const OUT_ADC2_L   = 0x0a;
// const OUT_ADC2_H   = 0x0b;
// const OUT_ADC3_L   = 0x0c;
// const OUT_ADC3_H   = 0x0d;
const OUT_X_L      = 0x28;
const OUT_X_H      = 0x29;
const OUT_Y_L      = 0x2a;
const OUT_Y_H      = 0x2b;
const OUT_Z_L      = 0x2c;
const OUT_Z_H      = 0x2d;

const RESOLUTION = 25;

class LIS3DH extends Sensor {
  constructor(options = {}) {
    super(options);
    this.address = options.addres || 0x18;
    this.name = 'LIS3DH';
    this._sensor = new i2c(this.address, {device: options.device || '/dev/i2c-1'});
    this._resolution = options.resolution || RESOLUTION;
  }

  _init() {
    return new Promise((resolve, reject) => {
      this._sensor.readBytes(WHO_AM_I, 1, (err, result) => {
        if (err) {
          reject(err);
        } else {
          this.enable().then(() => {
            return this.setHighResolution(true).then(() => {
              resolve();
            });
          }).catch(err => {
            reject(err);
          });
        }
      });
    });
  }

  enable() {
    return new Promise((resolve, reject) => {
      this._sensor.writeBytes( CTRL_REG1, [0x7f], err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  disable() {
    return new Promise((resolve, reject) => {
      this._sensor.writeBytes( CTRL_REG1, [0x00], err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  setHighResolution(flag) {
    return new Promise((resolve, reject) => {
      let cmd = (flag) ? 0x08 : 0x00;
      this._sensor.writeBytes( CTRL_REG4, cmd, err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  _read() {
    let xl = this._sensor.readBytes( OUT_X_L, 1, function(err, data) {});
    let xh = this._sensor.readBytes( OUT_X_H, 1, function(err, data) {});
    let yl = this._sensor.readBytes( OUT_Y_L, 1, function(err, data) {});
    let yh = this._sensor.readBytes( OUT_Y_H, 1, function(err, data) {});
    let zl = this._sensor.readBytes( OUT_Z_L, 1, function(err, data) {});
    let zh = this._sensor.readBytes( OUT_Z_H, 1, function(err, data) {});

    xl = xl.readInt8();
    xh = xh.readInt8();
    yl = yl.readInt8();
    yh = yh.readInt8();
    zl = zl.readInt8();
    zh = zh.readInt8();

    let x = ( (xh << 8) | xl ) >> 4;
    let y = ( (yh << 8) | yl ) >> 4;
    let z = ( (zh << 8) | zl ) >> 4;
    x /= 1024;
    y /= 1024;
    z /= 1024;

    return { x, y, z };
  }

  read() {
    return Bluebird.delay(this._resolution).then(() => {
      return this._read();
    });
  }
}

module.exports.LIS3DH = LIS3DH;
