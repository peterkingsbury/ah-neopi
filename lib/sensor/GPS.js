'use strict';

const Promise = require('bluebird');
const Sensor = require('../core/Sensor').Sensor;
const i2c = require('i2c-bus');

const I2C_BUS_NUMBER = 1;
const ARDUINO_I2C_SLAVE = 0x04;
// const ACCEPTABLE_TIME_DRIFT_THRESHOLD = 10000;
const I2C_BUFFER_LENGTH = 32;

const I2C_GPS_COMMAND = {
  COORDINATES: 0x01,
  SPEED:       0x02,
  ALTITUDE:    0x04,
  ANGLE:       0x08
};

/**
 * Wrapper class for I2C communication with Arduino (connected to GPS).
 */
class GPS extends Sensor {
  constructor(options) {
    super(options);
    this.name = 'GPS';
    this._wire = i2c.openSync(I2C_BUS_NUMBER);
    this._buffer = new Buffer(I2C_BUFFER_LENGTH);
    this._reading = {};
  }

  _init() {
    return Promise.resolve();
    return new Promise((resolve, reject) => {
      this._wire = i2c.open(I2C_BUS_NUMBER, {}, err => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
    });
  }

  _i2cSendCommand(cmd) {
    return new Promise((resolve, reject) => {
      this._wire.sendByte(ARDUINO_I2C_SLAVE, cmd, err => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
    });
  }

  _i2cRead() {
    this._buffer.fill(0);
    return new Promise((resolve, reject) => {
      this._wire.i2cRead(ARDUINO_I2C_SLAVE, I2C_BUFFER_LENGTH, this._buffer, (err, bytesRead, buffer) => {
        if (err) {
          return reject(err);
        }
        resolve(buffer);
      });
    });
  }

  _validateReading(reading) {
    return reading;
  }

  read() {
    return Promise.resolve().then(() => {
      return this._i2cSendCommand(I2C_GPS_COMMAND.COORDINATES).then(() => {
        return this._i2cRead().then(buffer => {
          let result = buffer.toString().split('|');
          this._reading.latitude = parseFloat(result[0]);
          this._reading.longitude = parseFloat(result[1]);
        });
      });
    }).then(() => {
      return this._i2cSendCommand(I2C_GPS_COMMAND.SPEED).then(() => {
        return this._i2cRead().then(buffer => {
          let result = buffer.toString().split('|');
          this._reading.speed = parseFloat(result[0]);
        });
      });
    }).then(() => {
      return this._i2cSendCommand(I2C_GPS_COMMAND.ALTITUDE).then(() => {
        return this._i2cRead().then(buffer => {
          let result = buffer.toString().split('|');
          this._reading.altitude = parseFloat(result[0]);
        });
      });
    }).then(() => {
      return this._i2cSendCommand(I2C_GPS_COMMAND.ANGLE).then(() => {
        return this._i2cRead().then(buffer => {
          let result = buffer.toString().split('|');
          this._reading.angle = parseFloat(result[0]);
        });
      });
    }).then(reading => {
      return this._validateReading(reading);
    }).then(() => {
      return this._reading;
    }).catch(err => {
      return this._reading;
    });
  }
}

module.exports.GPS = GPS;
