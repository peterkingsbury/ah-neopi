'use strict';

const CoreSensor = require('../core/Sensor').Sensor;

class Sensor extends CoreSensor {
  constructor(options) {
    super(options);
    this.name = 'Sensor';
    this.address = 0x00;
  }
}

module.exports.Sensor = Sensor;
