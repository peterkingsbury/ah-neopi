'use strict';

const Servo = require('./Servo').Servo;
const PIGPIO = require('js-pigpio');
const HOST = '127.0.0.1';
const PORT = 8888;
const PiGPIO = new PIGPIO();

class SG90 extends Servo {
  constructor(options) {
    super(options);
    this._pin = options.pin;
    this._min = 750;
    this._max = 2500;
  }

  _init() {
    return new Promise((resolve, reject) => {
      PiGPIO.pi(HOST, PORT, err => {
        if (err) {
          reject(err);
        } else {
          PiGPIO.set_PWM_dutycycle(this._pin, 255);
          this.center();
          resolve();
        }
      });
    });
  }

  _shutdown() {
    PiGPIO.set_PWM_dutycycle(this._pin, 0);
  }

  move(percent = 0.5) {
    if (percent < 0) {
      percent = 0;
    } else if (percent > 1) {
      percent = 1;
    }

    let pulseWidth = this._calculatePulseWidth(percent);
    PiGPIO.setServoPulsewidth(this._pin, pulseWidth);
  }

  center() {
    return this.move(0.5);
  }

  _calculatePulseWidth(percent) {
    let pulseWidth = ((this._max - this._min) * percent) + this._min;
    return parseInt(pulseWidth);
  }
}

module.exports.SG90 = SG90;

function test() {
  let pan = null;
  let tilt = null;
  function demo() {
    let SG90 = require('./SG90').SG90;
    pan = new SG90({pin: 12});
    tilt = new SG90({pin: 13});

    return Promise.all([
      pan._init(),
      tilt._init()
    ]).then(() => {
      let interval = setInterval(function () {

        pan.move(Math.random());
        tilt.move(Math.random());

      }, 1500);

      return interval;
    }).catch(err => {
      console.log(err);
    });
  }

  return demo();
}
