# NeoPi


## Setup the Environment

#### Add color to root console

```bash
$ sudo su -
# cp -a /etc/skel/.??* ~/
# exit
```

## Install Services

### GPSd

```bash
$ sudo apt-get install -y gpsd gpsd-clients python-gps
```

```bash
sudo tee /etc/default/gpsd <<EOF
START_DAEMON="true"
GPSD_OPTIONS="-n"
DEVICES="/dev/ttyAMA0" 
USBAUTO="true"
GPSD_SOCKET="/var/run/gpsd.sock"
EOF
```

```bash
$ sudo shutdown -r now
```

## Install Node.js

```bash
$ wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
$ bash
$ nvm install v6.10.0
$ sudo su -
# wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
# bash
# nvm install v6.10.0
```

## Start NeoPi on System Startup

### Install pm2

```bash
$ sudo su -
# npm install -g pm2
```

```json
{
  "apps" : [{
    "name"        : "NeoPi",
    "cwd"         : "/home/pi/Development/ah-neopi/",
    "script"      : "./node_modules/.bin/actionhero",
    "args"        : "start",
    "watch"       : true,
    "max_memory_restart": "180M"
  }]
}
```

```bash
$ pm2 install pm2-logrotate
$ pm2 set pm2-logrotate:compress true
$ pm2 start /home/pi/Development/ah-neopi/system/ah-neopi.pm2.json
$ pm2 save
$ crontab -e
```

```bash
@reboot /home/pi/.nvm/versions/node/v6.10.0/bin/node /home/pi/.nvm/versions/node/v6.10.0/bin/pm2 resurrect
```

## Install GPiGPIO

```bash
$ wget abyz.co.uk/rpi/pigpio/pigpio.zip
$ unzip pigpio.zip
$ cd PIGPIO
$ make
$ sudo make install
```

```bash
$ crontab -e
```

```bash
@reboot /usr/local/bin/pigpiod
```

## Install Python Tools

```bash
$ sudo apt-get install -y python-numpy python-scipy python-matplotlib
```
